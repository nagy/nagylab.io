// Three.js Library
// Homepage: https://github.com/mrdoob/three.js/tree/r136
// License: MIT See: https://github.com/mrdoob/three.js/blob/r136/LICENSE

customElements.define('three-renderer-advanced', class extends HTMLElement {

    renderer
    camera
    scene

    connectionPromiseResolver
    connectionPromise = new Promise( (resolve, reject) => { this.connectionPromiseResolver = resolve })

    async connectedCallback() {
        const THREE = await import("three")
        this.renderer = new THREE.WebGLRenderer( { antialias: true } );
        this.camera = new THREE.PerspectiveCamera( 45, 1.0, 0.01, 2000 );
        this.camera.position.set( 2.0, 0.0, 4.0 );

        this.renderer.setPixelRatio( window.devicePixelRatio )
        this.renderer.domElement.setAttribute('tabindex', 1)
        this.renderer.toneMapping = THREE.ACESFilmicToneMapping
        this.renderer.toneMappingExposure = 1
        this.renderer.shadowMap.enabled = true
        this.renderer.outputEncoding = THREE.sRGBEncoding
        this.appendChild ( this.renderer.domElement )
        this.renderer.setSize( this.renderer.domElement.clientWidth, this.renderer.domElement.clientHeight )
        this.camera.aspect = this.renderer.domElement.clientWidth / this.renderer.domElement.clientHeight
        this.camera.updateProjectionMatrix()
        this.animate()

        // let getRendererInt = (attr) => parseInt(getComputedStyle(renderer.domElement)[attr])
        // let updateSizes = function () {
        //     renderer.setSize( getRendererInt("width"), getRendererInt("height"), false )
        //     camera.aspect = getRendererInt("width")/ getRendererInt("height")
        //     camera.updateProjectionMatrix()
        // }
        // updateSizes()
        // const resizeObserver = new ResizeObserver(entries => updateSizes());
        // resizeObserver.observe(renderer.domElement);
        this.connectionPromiseResolver()
    }

    get childScene() {
        if (this.scene) {
            return this.scene
        }
        for(let ch of this.children) {
            if (ch.scene) {
                return ch
            }
        }
    }

    animate() {
        requestAnimationFrame( () => { this.animate() } );
        let childSc = this.childScene
        if (childSc){
            this.renderer.render( childSc.scene, this.camera );
            childSc?.update?.()

            try {
                const diff = (attr) => childSc.scene.children[0].position[attr] - this.camera.position[attr]
                this.camera.position.x += diff("x")/5
                this.camera.position.y += diff("y")/5
            } catch (e) {

            }
        }
    }

})
