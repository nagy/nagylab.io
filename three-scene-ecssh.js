// Three.js Library
// Homepage: https://github.com/mrdoob/three.js/tree/r136
// License: MIT See: https://github.com/mrdoob/three.js/blob/r136/LICENSE
import { WebSocketInit, wsSend } from './cl/ecssh.js';
import { wsSend, wsSetPositions, wsSetRotations } from './cl/ecssh.js';

customElements.define('three-scene-ecssh', class extends HTMLElement {

    scene

    connectionPromiseResolver
    connectionPromise = new Promise( (resolve, reject) => { this.connectionPromiseResolver = resolve })

    async connectedCallback() {
        const THREE = await import("three")
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color('black');

        // TODO use url from attribute
        WebSocketInit((instr) => { this.applyInstruction(instr) })
        await this.parentElement.connectionPromise
        // addKeys
        const addEl = this.parentElement.renderer.domElement.addEventListener
        addEl("keydown", (ev) => wsSend("set :root :k"+ev.key+ " t"), false);
        addEl("keyup",   (ev) => wsSend("set :root :k"+ev.key+ " nil"), false)
        addEl("keydown", (ev) => { if (ev.key == "l") this.addLight() } )
        this.connectionPromiseResolver()
        wsSend("get-urllist")
    }

    update() {
        this.light?.target.updateMatrixWorld();
        this.lighthelper?.update()
        wsSend("gena")
    }

    async addLight() {
        const THREE = await import("three")
        const light = new THREE.DirectionalLight( 0xffffff, 1 );
        light.castShadow = true;
        light.position.set(-5, 5, 5);
        // light.position.set(0, 20, 0);
        light.target.position.set(0, 0, -4);
        // light.target.position.set(0, 0, 0);
        light.shadow.mapSize.width = 512 * 8;
        light.shadow.mapSize.height = 512 * 8;
        light.shadow.bias = -0.00004; // fixes artifacts https://threejs.org/docs/#api/en/lights/shadows/LightShadow.bias
        light.shadow.camera.near = 0.5; // default
        light.shadow.camera.far = 500; // default
        this.light = light
        const lighthelper = new THREE.DirectionalLightHelper(light);
        this.lighthelper = lighthelper
        this.scene.add(lighthelper);
        this.scene.add(light);
        this.scene.add(light.target);
        const cameraHelper = new THREE.CameraHelper(light.shadow.camera);
        this.scene.add(cameraHelper);
    }

    applyInstruction (instr) {
        if (instr[0] == "DECF") {
            //console.log("we are decrementing something", instr[1], instr[2])
            if ( instr[1][0] == "C") { // a component
                const entity = instr[1][1]
                const value = instr[2]
                if (instr[1][2]==":PX") this.scene.children[entity].position.x -= value;
                else if (instr[1][2]==":PY") this.scene.children[entity].position.y -= value;
                else if (instr[1][2]==":PZ") this.scene.children[entity].position.z -= value;
                else if (instr[1][2]==":RX") this.scene.children[entity].rotation.x -= value;
                else if (instr[1][2]==":RY") this.scene.children[entity].rotation.y -= value;
                else if (instr[1][2]==":RZ") this.scene.children[entity].rotation.z -= value;
            }
        } else if (instr[0] == "INCF") {
            //console.log("we are incrementing something", instr[1], instr[2])
            if ( instr[1][0] == "C") { // a component
                const entity = instr[1][1]
                const value = instr[2]
                if (instr[1][2]==":PX") this.scene.children[entity].position.x += value;
                else if (instr[1][2]==":PY") this.scene.children[entity].position.y += value;
                else if (instr[1][2]==":PZ") this.scene.children[entity].position.z += value;
                else if (instr[1][2]==":RX") this.scene.children[entity].rotation.x += value;
                else if (instr[1][2]==":RY") this.scene.children[entity].rotation.y += value;
                else if (instr[1][2]==":RZ") this.scene.children[entity].rotation.z += value;
            }
        } else if (instr[0] == "LOAD-GLTF"){
            console.log("loading gltf", instr[1])
            this.addGLTF(JSON.parse(instr[1]))
        }
    }

    async loadGLTF (url) {
        const GLTFLoaderModule = await import ("three/examples/jsm/loaders/GLTFLoader.js")
        const loader = new GLTFLoaderModule.GLTFLoader();
        return new Promise((resolve, reject) => loader.load( url, resolve, null , reject))
    };
    async addGLTF(url) {
        let gltf = await this.loadGLTF(url)
        gltf.scene.castShadow = true;
        gltf.scene.receiveShadow = true;
        this.scene.add(gltf.scene)
        let setMeshMat = function(mesh) {
            mesh.castShadow = true;
            mesh.receiveShadow = true;
        };
        for(const ch of this.scene.children) {
            if (ch.type == "Mesh") {
                setMeshMat(ch)
            } else if (ch.type == "Group") {
                for(const ch2 of ch.children) {
                    if (ch2.type == "Mesh") {
                        setMeshMat(ch2)
                    }
                }
            }
        }
        wsSetPositions(this.scene.children.length-1, this.scene.children.at(-1).position)
        wsSetRotations(this.scene.children.length-1, this.scene.children.at(-1).rotation)
    }

})
