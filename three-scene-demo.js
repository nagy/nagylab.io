// Three.js Library
// Homepage: https://github.com/mrdoob/three.js/tree/r136
// License: MIT See: https://github.com/mrdoob/three.js/blob/r136/LICENSE

customElements.define('three-scene-demo', class extends HTMLElement {

    #scene
    #cube

    async connectedCallback() {
        const THREE = await import("three")
        if (this.#scene) {
            return // do not recreate scene
        }
        this.#scene = new THREE.Scene();
        const geometry = new THREE.BoxGeometry();
        let color = this.getAttribute("cube-color") || 0x00ffff;
        const material = new THREE.MeshBasicMaterial( { color } );
        this.#cube = new THREE.Mesh( geometry, material );
        this.#scene.add( this.#cube );
    }

    get scene() { return this.#scene; }

    update() {
        if(this.#cube) {
            this.#cube.rotation.x += 0.01;
            this.#cube.rotation.y += 0.01;
        }
    }

})
