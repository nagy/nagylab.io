// Three.js Library
// Homepage: https://github.com/mrdoob/three.js/tree/r136
// License: MIT See: https://github.com/mrdoob/three.js/blob/r136/LICENSE

customElements.define('three-renderer', class extends HTMLElement {

    #renderer
    #camera

    async connectedCallback() {
        const THREE = await import("three")
        this.#renderer = new THREE.WebGLRenderer();
        this.#camera = new THREE.PerspectiveCamera( 75, 1 , 0.1, 1000 );
        this.#camera.position.z = 5;

        this.#renderer.setPixelRatio( window.devicePixelRatio )
        this.appendChild ( this.#renderer.domElement )
        this.#renderer.setSize( this.#renderer.domElement.clientWidth, this.#renderer.domElement.clientHeight )
        this.#camera.aspect = this.#renderer.domElement.clientWidth / this.#renderer.domElement.clientHeight
        this.#camera.updateProjectionMatrix()
        this.animate()
    }

    get childScene() {
        for(let ch of this.children) {
            if (ch.scene) {
                return ch
            }
        }
    }

    animate() {
        requestAnimationFrame( () => { this.animate() } );
        let childSc = this.childScene
        if (childSc){
            this.#renderer.render( childSc.scene, this.#camera );
            childSc.update()
        }
    }

})
