// Three.js Library
// Homepage: https://github.com/mrdoob/three.js/tree/r136
// License: MIT See: https://github.com/mrdoob/three.js/blob/r136/LICENSE

customElements.define('three-orbitcontroller', class extends HTMLElement {

    async connectedCallback() {
        const OrbitControlsModule = await import ("three/examples/jsm/controls/OrbitControls.js")
        const camera = this.parentElement.camera
        const renderer = this.parentElement.renderer
        const controls = new OrbitControlsModule.OrbitControls( camera, renderer.domElement );
        controls.target.set( 0, 0, 0 );
        controls.update();
    }

})
